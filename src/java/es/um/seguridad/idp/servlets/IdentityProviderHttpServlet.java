package es.um.seguridad.idp.servlets;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import es.um.seguridad.adaptadores.MySQLAdapter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.servlet.http.HttpSession;



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObject;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 *
 * @author jgd
 */
@WebServlet(urlPatterns = {"/idp"})
public class IdentityProviderHttpServlet extends HttpServlet {
    

    private static String IDENTIFICATION = "http://localhost:8080/seg-idp/id";
    private static String CREATE_RESPONSE = "http://localhost:8080/seg-idp/createResponse";

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            DefaultBootstrap.bootstrap();
        } catch (ConfigurationException ex) {
            Logger.getLogger(IdentityProviderHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        PrintWriter out = null;


        /*       MySQLAdapter adapter = new MySQLAdapter("root", "alumno", "192.168.1.15:3306/users");
         String date = new Date().toString();
         */


        String samlAuthRequest = request.getParameter("SAMLAuthRequest");

        if (samlAuthRequest == null) {
            // Mostrar mensaje de error "No viene de un sp válido"

            try {
                response.setContentType("text/html;charset=UTF-8");
                out = response.getWriter();


                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Identificación incorrecta</title>");
                out.println("</head>");
                out.println("<body>");;
                out.println("<p>No traes SAML, no puedo ayudarte</p>");

                out.println("</body>");
                out.println("</html>");

            } catch (IOException ex) {
                Logger.getLogger(IdentityProviderHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();

            }
        } else {

            System.out.print(samlAuthRequest);
            samlAuthRequest = samlAuthRequest.replaceAll("\'", "\"");

            System.out.print(samlAuthRequest);

            // Datos a obtener del authRequest
            String id, issuerID, assertionURL, issuerName, destino;

            //Creamos el authRequest para darle los datos a la sesion
            AuthnRequest authnRequest = null;


            try {

                authnRequest = (AuthnRequest) stringToSAML(samlAuthRequest);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(IdentityProviderHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (XMLParserException ex) {
                Logger.getLogger(IdentityProviderHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnmarshallingException ex) {
                Logger.getLogger(IdentityProviderHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }

            id = authnRequest.getID();
            issuerID = authnRequest.getIssuer().getValue();
            assertionURL = authnRequest.getAssertionConsumerServiceURL();
            issuerName = authnRequest.getIssuer().getNameQualifier();
            destino = authnRequest.getDestination();
            String samlResponseString = request.getParameter("SAMLResponse");

            // TODO: Podria hacer falta true
            HttpSession httpSession = request.getSession(true);
            // TODO: Podria hacer falta quitar comillas simples por \"

            // Establecemos los datos anteriores en la sesion

            httpSession.setAttribute("id", id);
            httpSession.setAttribute("issuerID", issuerID);
            httpSession.setAttribute("assertionURL", assertionURL);
            httpSession.setAttribute("issuerName", issuerName);
            httpSession.setAttribute("destino", destino);


            if (samlResponseString == null) {
                
                // Redirigir a IDENTIFICATION para verificar las credenciales 
                redirect(IDENTIFICATION, request, response);

            } else {
                
                // El usuario ya ha sido identificado antes
                redirect(CREATE_RESPONSE, request, response);
            }
        }




    }

    private void redirect(String url, HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;

        try {
            response.setContentType("text/html;charset=UTF-8");
            out = response.getWriter();


            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Identificación correcta</title>");
            out.println("</head>");
            out.println("<body>");;
            out.println("<form id=\"form\" action=\"" + url + "\" method=\"post\">");
            out.println("<script> var formulario = document.getElementById('form'); formulario.submit(); </script>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            Logger.getLogger(IdentityProviderHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();

        }

    }

    /*Ejemplo de formulario
     *          * 
     <html>
     <head><title>Login de usuarios</title>
     </head>
     <body>

     <form action="login.php" method="POST">
     Usuario: <input type="text" name="usuario"><br>
     Password: <input type="password" name="pass"><br>
     <input type="submit" value="Entrar">
     </form>

     ¿No estás registrado? <a href="registro.php">Regístrate</a>.
     </body>
     </html>
     */
    private String md5(String clear) {

        StringBuffer sb = new StringBuffer();

        try {

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] b = md.digest(clear.getBytes());
            int size = b.length;

            //convert the byte to hex format method 1
            sb = new StringBuffer();
            for (int i = 0; i < size; i++) {
                sb.append(Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1));





            }

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(MySQLAdapter.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString();

    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            processRequest(request, response);
        
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private SAMLObject stringToSAML(String samlObject) throws UnsupportedEncodingException, XMLParserException, UnmarshallingException {
        System.out.print("stringSAML: " + samlObject);

        System.out.println("asñldfkasd-lfj");
         InputStream is = new ByteArrayInputStream(samlObject.getBytes());
        BasicParserPool parser = new BasicParserPool();
        Document doc = parser.parse(is);
        Element samlElement = doc.getDocumentElement();
        UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
        Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(samlElement);
        return (SAMLObject) unmarshaller.unmarshall(samlElement);
    }
}
