/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.um.seguridad.idp.servlets;

import es.um.seguridad.adaptadores.MySQLAdapter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jgd
 */
@WebServlet(name = "IdentificationHttpServlet", urlPatterns = {"/id"})
public class IdentificationHttpServlet extends HttpServlet {

    private static String USER_DB = "alumno";
    private static String PASSWORD_DB = "alumno";
    private static String HOST_DB = "192.168.1.16:3306/users";
    private static String IDENTIFICAR = "http://localhost:8080/seg-idp/id";
    private static String CREATE_RESPONSE = "http://localhost:8080/seg-idp/createResponse";
    private static MySQLAdapter adapter = new MySQLAdapter(USER_DB, PASSWORD_DB, HOST_DB);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        String user = request.getParameter("usuario");

        if (user == null) {

            // Mostramos formulario para identificacion
            muestraFormularioIdentificar(response);

        } else {

            if (checkPassword(user, request.getParameter("pass"), request.getParameter("date"))) {
                // Se trata de un usuario legitimo del sistema. Se autoriza su entrada
                redirect(CREATE_RESPONSE, response);
            } else {

                // La autenticación no se ha realizado correctamente

                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Identificación errónea</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>El usuario o la contraseña son incorrectos</h1>");
                out.println("</body>");
                out.println("</html>");

            }
        }
    }

    private void redirect(String url, HttpServletResponse response) {
        PrintWriter out = null;

        try {
            response.setContentType("text/html;charset=UTF-8");
            out = response.getWriter();


            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Identificación correcta</title>");
            out.println("</head>");
            out.println("<body>");;
            out.println("<form id=\"form\" action=\"" + url + "\" method=\"post\">");
            out.println("<script> var formulario = document.getElementById('form'); formulario.submit(); </script>");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            Logger.getLogger(IdentityProviderHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();

        }

    }

    /**
     * Muestra formulario para autenticacion del usuario
     *
     * @param response Respuesta donde se mandaran las credenciales
     */
    private void muestraFormularioIdentificar(HttpServletResponse response) {
        PrintWriter out = null;


        try {
            response.setContentType("text/html;charset=UTF-8");
            out = response.getWriter();


            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Necesita usuario y contraseña</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<form id=\"form\" action=\"" + IDENTIFICAR + "\" method=\"post\">");

            out.println("Usuario: <input type=\"text\" name=\"usuario\"><br>");
            out.println(" Password: <input type=\"password\" name=\"pass\"><br>");

            out.println("<input type=\"submit\" value=\"Entrar\">");
            out.println("</body>");
            out.println("</html>");

        } catch (IOException ex) {
            Logger.getLogger(IdentityProviderHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();

        }

    }

    /**
     * Comprueba la validez del usuario y la contraseña
     *
     * @param user Usuario
     * @param password Contraseña
     * @return True si la combinacion es correcta y false en caso contrario
     */
    private boolean checkPassword(String user, String password, String date) {


        if (user == null || password == null) {
            throw new IllegalArgumentException("El usuario o la contraseña no pueden ser nulos");
        }


        //return adapter.checkPassword(user, password, date);

        return user.equals("alumno") && password.equals("alumno");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
