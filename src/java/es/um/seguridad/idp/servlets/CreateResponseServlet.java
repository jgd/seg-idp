/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.um.seguridad.idp.servlets;

import es.um.seguridad.idp.util.SAMLInputContainer;
import es.um.seguridad.idp.util.SAMLWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.joda.time.DateTime;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Status;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.StatusMessage;
import org.opensaml.saml2.core.impl.IssuerBuilder;
import org.opensaml.saml2.core.impl.StatusCodeBuilder;
import org.opensaml.saml2.core.impl.StatusMessageBuilder;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.util.XMLHelper;

/**
 *
 * @author jgd
 */
@WebServlet(name = "CreateResponseServlet", urlPatterns = {"/createResponse"})
public class CreateResponseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            DefaultBootstrap.bootstrap();
        } catch (ConfigurationException ex) {
            Logger.getLogger(CreateResponseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }



        HttpSession httpSession = request.getSession();
        if (httpSession != null) {


            SAMLInputContainer samlContainter = new SAMLInputContainer();

            samlContainter.setStrIssuer((String) httpSession.getAttribute("issuerID"));
            samlContainter.setStrNameQualifier((String) httpSession.getAttribute("issuerName"));
            samlContainter.setSessionId((String) httpSession.getAttribute("id"));

            // TODO: Guardar en httpSession
            samlContainter.setStrNameID("alumno");
            samlContainter.setStrNameQualifier("pruebaDominio");

            // Creamos el assertion a partir del contenedor con los datos que hemos rellenado antes
            List<Assertion> listaAssertion = new LinkedList<Assertion>();
            listaAssertion.add(SAMLWriter.buildDefaultAssertion(samlContainter));


            // Ya podemos crear la respuesta para redirigirla al Sp
            Response responseSAML = createResponse(samlContainter.getStrIssuer(), samlContainter.getSessionId(), (String) httpSession.getAttribute("destino"), listaAssertion);

            String responseSAMLString = null;
            try {
                responseSAMLString = SAMLtoString(responseSAML);
            } catch (MarshallingException ex) {
                Logger.getLogger(CreateResponseServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            responseSAMLString = responseSAMLString.replaceAll("\"", "'");

            // TODO: Cambiar ruta para redireccion
            // Una vez aqui ya solo tenemos que redirigir la respuesta hacia el sp que ha solicitado el samls

            try {
                response.setContentType("text/html;charset=UTF-8");
                out = response.getWriter();

                System.out.println("ASSERTION URL: " + (String) httpSession.getAttribute("assertionURL"));

                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Identificación correcta</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<form id=\"form\" action=\"" + (String) httpSession.getAttribute("assertionURL") + "\" method=\"post\">");
                // out.println("<form id=\"form\" action=\"" + "http://localhost:8080/seg-sp/sp/private" + "\" method=\"post\">");

                out.println("<input type=\"hidden\" name=\"responseSAML\"value=\"" + responseSAMLString + "\"/>");
                out.println("<script> var formulario = document.getElementById('form'); formulario.submit(); </script>");
                out.println("</body>");
                out.println("</html>");

            } catch (IOException ex) {
                Logger.getLogger(IdentityProviderHttpServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();

            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String SAMLtoString(XMLObject object) throws MarshallingException {
        MarshallerFactory marshallerFactory = org.opensaml.Configuration.getMarshallerFactory();
        org.opensaml.xml.io.Marshaller marshaller = marshallerFactory.getMarshaller(object);
        org.w3c.dom.Element subjectElement = marshaller.marshall(object);
        return XMLHelper.prettyPrintXML(subjectElement);
    }

    @SuppressWarnings("rawtypes")
    public Response createResponse(String issuerId, String requestId, String destination, List<Assertion> assertions) {
        XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
        // Create Response
        SAMLObjectBuilder builder = (SAMLObjectBuilder) builderFactory.getBuilder(Response.DEFAULT_ELEMENT_NAME);
        Response response = (Response) builder.buildObject();
        // Set request Id
        response.setInResponseTo(requestId);
        // Set Issuer
        Issuer issuer = new IssuerBuilder().buildObject();
        issuer.setValue("http://idp.um:8080");
        response.setIssuer(issuer);
        response.setIssueInstant(new DateTime());

        // Set status code and message
        StatusCode statusCode = new StatusCodeBuilder().buildObject();
        statusCode.setValue(StatusCode.SUCCESS_URI);
        StatusMessage statusMessage = new StatusMessageBuilder().buildObject();
        statusMessage.setMessage("OK");
        builder = (SAMLObjectBuilder) builderFactory.getBuilder(Status.DEFAULT_ELEMENT_NAME);
        Status responseStatus = (Status) builder.buildObject();
        responseStatus.setStatusCode(statusCode);
        responseStatus.setStatusMessage(statusMessage);
        response.setStatus(responseStatus);

        // Include assertions
        response.getAssertions().addAll(assertions);
        // Set destination
        response.setDestination(destination);
        return response;
    }
}
